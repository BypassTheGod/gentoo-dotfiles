import subprocess,requests,os,psutil
from time import sleep

print("=" * 40)
print("Welcome To My Gentoo Install Script")
print("=" * 40)


## Creates Function to Download Stage 3 Tar ball of users choice
def stage3_select():

    subprocess.run(["clear"])
    print("1: Base Stage 3\n2: Hardended Stage 3\n")

    stage3_selection = int(input("Please Specify a number 1-2: "))


    if stage3_selection == 1:
        stage3_path_url="http://distfiles.gentoo.org/releases/amd64/autobuilds/latest-stage3-amd64-openrc.txt"
        stage3_path = subprocess.run(f'curl -s {stage3_path_url} | grep -v "^#" | cut -d" " -f1',shell=True,stdout=subprocess.PIPE)
        stage3_path_decode = stage3_path.stdout.decode()
        stage3_url = f"http://distfiles.gentoo.org/releases/amd64/autobuilds/{stage3_path_decode}"
        subprocess.run(f'wget -P /mnt/gentoo/ {stage3_url}',shell=True)
    elif stage3_selection == 2:
        stage3_path_url="http://distfiles.gentoo.org/releases/amd64/autobuilds/latest-stage3-amd64-hardened-openrc.txt"
        stage3_path = subprocess.run(f'curl -s {stage3_path_url} | grep -v "^#" | cut -d" " -f1',shell=True,stdout=subprocess.PIPE)
        stage3_path_decode = stage3_path.stdout.decode()
        stage3_url = f"http://distfiles.gentoo.org/releases/amd64/autobuilds/{stage3_path_decode}"
        subprocess.run(f'wget -P /mnt/gentoo/ {stage3_url}',shell=True)
        print("Finished Downloading Hardended Stage3")



#Selects the drive to install to
def select_drive():
    subprocess.run('clear')
    subprocess.run("lsblk")
    print("=" * 45)
    print("Please Select a Drive to install Gentoo onto")
    print("=" * 45)
    global drive_selection
    drive_selection = input("Please select the drive name witohut /dev/: ")
    verify_drive = input(f"Are you sure this is the correct drive /dev/{drive_selection}: ")
    if verify_drive == "yes".lower():
        print(f"this is about to wipe all data on {drive_selection}... This is your last change to kill the script before perm data loss.")
        sleep(5)
        subprocess.run(f'wipefs -a /dev/{drive_selection}',shell=True ,capture_output=True)
        subprocess.run(f'parted -a optimal /dev/{drive_selection} --script mklabel gpt',shell=True)
        subprocess.run(f'parted -a optimal /dev/{drive_selection} --script mkpart primary 1 2000',shell=True)
        subprocess.run(f'parted -a optimal /dev/{drive_selection} --script name 1 grub',shell=True)
        subprocess.run(f'parted -a optimal /dev/{drive_selection} --script mkpart primary 2000 100000',shell=True)
        subprocess.run(f'parted -a optimal /dev/{drive_selection} --script name 2 rootfs',shell=True)
        subprocess.run(f'parted -a optimal /dev/{drive_selection} --script -- mkpart primary 100000 -1 ',shell=True)
        subprocess.run(f'parted -a optimal /dev/{drive_selection} --script name 3 home ',shell=True)
        subprocess.run("clear",shell=True)
        print("=" * 40)
        print("Formating The Drives now")
        print("=" * 40)
        subprocess.run(f"mkfs.fat -F32 /dev/{drive_selection}1",shell=True)
        subprocess.run(f"mkfs.ext4 /dev/{drive_selection}2",shell=True)
        subprocess.run(f"mkfs.ext4 /dev/{drive_selection}3",shell=True)
        subprocess.run("mkdir -p /mnt/gentoo/",shell=True)
        subprocess.run("clear")
        subprocess.run(f"mount /dev/{drive_selection}2 /mnt/gentoo/",shell=True)
        stage3_select()



    elif verify_drive == "no".lower():
        print("Exiting Script")
        exit()
select_drive()




def pre_chroot():
    os.chdir("/mnt/gentoo/")
    subprocess.run("pwd",shell=True)
    subprocess.run("tar xpvf stage3-*.tar.xz --xattrs-include='*.*' --numeric-owner",shell=True)
    subprocess.run("clear",shell=True)

    make_conf_choice = int(input("Would you like to use my Make.conf or generate a custom one\n1: My Make.conf\n2: Generate a make.conf: "))

    if make_conf_choice == 1:
        subprocess.run("wget -P /mnt/gentoo/ https://gitlab.com/BypassTheGod/gentoo-dotfiles/-/raw/main/make.conf",shell=True)
        subprocess.run("mv /mnt/gentoo/make.conf /mnt/gentoo/etc/portage/",shell=True)

    elif make_conf_choice == 2:
        print("=" * 150)
        print("Generating a make.conf Does not set any USE flags please be aware of this as setting USE flags after inital setup would require to run emerge @world")
        print("=" * 150)
        f = open("/mnt/gentoo/etc/portage/make.conf", 'a')
        f.write(f'MAKEOPTS="-j{psutil.cpu_count(logical=True)} -l{psutil.cpu_count(logical=True)}"\n')
        f.write(f'ACCEPT_KEYWORDS="~amd64"\n')
        f.write(f'FEATURES="binpkg-logs buildpkg cgroup collision-protect downgrade-backup ipc-sandbox network-sandbox parallel-fetch parallel-install sign candy"\n')
        f.write('PORTAGE_IONICE_COMMAND="ionice -c 3 -p \${PID}"\n')
        f.write('EMERGE_DEFAULT_OPTS="--ask-enter-invalid --jobs=20 --load-average=20"\n')
        f.close()






    subprocess.run("mkdir --parents /mnt/gentoo/etc/portage/repos.conf",shell=True)
    subprocess.run("cp /mnt/gentoo/usr/share/portage/config/repos.conf /mnt/gentoo/etc/portage/repos.conf/gentoo.conf",shell=True)
    subprocess.run("cp --dereference /etc/resolv.conf /mnt/gentoo/etc/",shell=True)
    subprocess.run("mount --types proc /proc /mnt/gentoo/proc",shell=True)
    subprocess.run("mount --rbind /sys /mnt/gentoo/sys",shell=True)
    subprocess.run("mount --make-rslave /mnt/gentoo/sys",shell=True)
    subprocess.run("mount --rbind /dev /mnt/gentoo/dev",shell=True)
    subprocess.run("mount --make-rslave /mnt/gentoo/dev",shell=True)
    subprocess.run("mount --bind /run /mnt/gentoo/run",shell=True)
    subprocess.run("mount --make-slave /mnt/gentoo/run",shell=True)
    os.chroot("/mnt/gentoo/")
pre_chroot()


def inital_post_chroot():
    subprocess.run("source /etc/profile",shell=True)

    subprocess.run(f"mount /dev/{drive_selection}1 /boot/",shell=True)
    subprocess.run(f"mount /dev/{drive_selection}3 /home/",shell=True)
    print("=" * 40)
    print("Starting emerge-webrsync")
    print("=" * 40)
    subprocess.run("emerge-webrsync",shell=True)

    print("=" * 60)
    print("Emergeing @world set next this could take a few hours")
    print("=" * 60)
    #subprocess.run("emerge --verbose --update --deep --newuse @world",shell=True)

    timezone_set = int(input("1: America/Chicago\n2: America/New_York\n3: America/Los_Angeles\n: "))

    if timezone_set == 1:
        file = open("/etc/timezone",'w')
        flie.write("America/Chicago")
        file.close()
        subprocess.run("emerge --config sys-libs/timezone-data",shell=True)
    elif timezone_set == 2:
        file = open("/etc/timezone", 'w')
        file.write("America/New_York")
        file.close()
        subprocess.run("emerge --config sys-libs/timezone-data",shell=True)
    elif timezone_set == 3:
        file = open("/etc/timezone", 'w')
        file.write("America/Los_Angeles")
        file.close()
        subprocess.run("emerge --config sys-libs/timezone-data",shell=True)
inital_post_chroot()


def inital_chroot_configuration():
    print("=" * 40)
    print("Time To Set Locale on the system")
    print("=" * 40)

    locale_set = int(input("1: en_US.UTF-8 UTF-8\n2: Default\n: "))

    if locale_set == 1:
        f = open("/etc/locale.gen",'a')
        f.write("en_US.UTF-8 UTF-8")
        f.close()
        subprocess.run("eselect locale set 4",shell=True)
        subprocess.run("locale-gen",shell=True)
    elif locale_set == 2:
        subprocess.run("locale-gen",shell=True)

inital_chroot_configuration()




def kernel_setup():
    subprocess.run("clear",shell=True)

    print("=" * 30)
    print("Time to Configure the Kernel")
    print("=" * 30)


    kernel_choice = int(input("1:Custom Configuration of kernel\n2:Use My Custom Kernel\n3:Use Gentoo Binary Pre configured Kernel\n:"))


    if kernel_choice == 1:
        f = open("/etc/portage/package.license",'a')
        f.write("sys-kernel/linux-firmware linux-fw-redistributable no-source-code")
        f.close()
        subprocess.run("emerge sys-kernel/linux-firmware sys-kernel/gentoo-sources sys-apps/pciutils sys-kernel/genkernel",shell=True)
        subprocess.run("eselect kernel set 1",shell=True)
        os.chdir("/usr/src/linux/")
        subprocess.run("make nconfig",shell=True)
        subprocess.run(f"make -j{psutil.cpu_count(logical=True)} && make modules_install && make install",shell=True)
        subprocess.run("genkernel --install --kernel-config=/usr/src/linux/.config initramfs",shell=True)
    elif kernel_choice == 2:
        f = open("/etc/portage/package.license",'a')
        f.write("sys-kernel/linux-firmware linux-fw-redistributable no-source-code")
        f.close()
        subprocess.run("emerge sys-kernel/linux-firmware sys-kernel/gentoo-sources sys-apps/pciutils sys-kernel/genkernel app-arch/lz4 lzop",shell=True)
        subprocess.run("wget https://gitlab.com/BypassTheGod/gentoo-dotfiles/-/raw/main/5.15.2-gentoo.config",shell=True)
        subprocess.run("eselect kernel set 1",shell=True)
        subprocess.run("mv 5.15.2-gentoo.config /usr/src/linux/.config",shell=True)
        os.chdir("/usr/src/linux/")
        subprocess.run(f"make -j{psutil.cpu_count(logical=True)} && make modules_install && make install",shell=True)
        subprocess.run("genkernel --install --kernel-config=/usr/src/linux/.config initramfs",shell=True)
    elif kernel_choice == 3:
        f = open("/etc/portage/package.license",'a')
        f.write("sys-kernel/linux-firmware linux-fw-redistributable no-source-code")
        f.close()
        subprocess.run("emerge sys-kernel/gentoo-kernel-bin",shell=True)
        subprocess.run("eselect kernel set 1",shell=True)
        subprocess.run("genkernel --install --kernel-config=/usr/src/linux/.config initramfs",shell=True)



kernel_setup()

def fstab_setup():
    print("=" * 40)
    print("Configuring the fstab")
    print("=" * 40)
    get_drives_uuid1 = subprocess.run(["blkid", "-s", "UUID", "-o", "value", f"/dev/{drive_selection}1"],stdout=subprocess.PIPE)
    get_drives_uuid2 = subprocess.run(["blkid", "-s", "UUID", "-o", "value", f"/dev/{drive_selection}2"],stdout=subprocess.PIPE)
    get_drives_uuid3 = subprocess.run(["blkid", "-s", "UUID", "-o", "value", f"/dev/{drive_selection}3"],stdout=subprocess.PIPE)


    f = open('/etc/fstab','a')
    f.write(f"UUID={get_drives_uuid1.stdout.decode('utf-8').rstrip()} /boot/ vfat defaults 0 2\n")
    f.write(f"UUID={get_drives_uuid2.stdout.decode('utf-8').rstrip()} / ext4 noatime 0 1\n")
    f.write(f"UUID={get_drives_uuid3.stdout.decode('utf-8').rstrip()} /home/ ext4 noatime 0 2\n")
    f.close()

fstab_setup()



def grub_configuration():
    print("=" * 25)
    print("Installin And Configuring Grub!")
    print("=" * 25)

    subprocess.run(['emerge','grub:2'])
    file = open('/etc/portage/make.conf','a')
    file.write('GRUB_PLATFORMS="efi-64"')
    file.close()
    subprocess.run(["grub-install", '--target=x86_64-efi', '--efi-directory=/boot/'])
    subprocess.run(["grub-mkconfig", '-o', '/boot/grub/grub.cfg'])
    subprocess.run(['clear'])
    print("="*120)
    print("The System Is Now Fully Installed and It's now time to configure The Network and user info")
    print("="*120)

grub_configuration()



def system_configuration():
    print("=" * 40)
    print("Setting User Info and Network Information")
    print("=" * 40)

    hostname = input("Please type what hostname you would like: ")
    hostname_file = open("/etc/conf.d/hostname",'w')
    hostname_file.write(f'hostname="{hostname}"')
    hostname_file.close()


    subprocess.run(['ifconfig'])
    network_dev = input("Please Specify your network device name: ")

    network_file = open('/etc/conf.d/net','w')
    network_file.write(f'config_{network_dev}="dhcp"')
    network_file.close()
    subprocess.run(["emerge","--noreplace","net-misc/netirfc"])
    os.chdir("/etc/init.d/")
    subprocess.run(["ln","-s","net.lo",f"net.{network_dev}"])
    subprocess.run(["rc-update","add",f"net.{network_dev}","default"])
    subprocess.run(["emerge","dhcpcd"])


    subprocess.run(["clear"])
    print("=" * 50)
    print("time to to set your root users password")
    print("=" * 50)

    subprocess.run(["passwd"])


    print("=" * 50)
    print("time to add a user account")
    print("=" * 50)
    username = input("Please Specify a username for your account: ")
    subprocess.run(['useradd','-m','-G','user,wheel,audio,video',f"{username}"])
    subprocess.run(['emerge','doas'])
    file = open("/etc/doas.conf",'w')
    file.write("permit :wheel")
    file.close()

system_configuration()



def finalizing():
    subprocess.run(["clear"])
    print("=" * 180)
    print("That's It this is the end of the install everyting else after this point is extra you may now reboot the system and boot into your new Gentoo/Linux System Enjoy!")
    print("=" * 180)
finalizing()
